FROM python:3.8-slim

RUN apt-get update; \
    apt-get install -y --no-install-recommends curl jq mime-support python3-yaml; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*

RUN pip install squad-client tuxsuite
