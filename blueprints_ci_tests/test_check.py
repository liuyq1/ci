from unittest import TestCase
from unittest.mock import Mock, patch
from blueprints_ci.check import check
from collections import namedtuple


class TestCheck(TestCase):

    def sample_settings(self):
        return Mock(
            SQUAD_JOB_ID="123",
            SQUAD_HOST="https://squad.example",
        )

    @patch("blueprints_ci.check.fetch_tests_from_squad")
    def test_basics(self, fetch_tests_from_squad_mock):
        Test = namedtuple("Test", ["name", "status"])
        fetch_tests_from_squad_mock.return_value = [Test("suite/test1", "pass"), Test("suite/test2", "pass")]
        settings = self.sample_settings()
        self.assertTrue(check(settings))

        # Make sure to return False if there is at least one failed test
        fetch_tests_from_squad_mock.return_value = [Test("suite/test1", "pass"), Test("suite/test2", "fail")]
        self.assertFalse(check(settings))

        # Make sure to return False if testjob didn't return any tests
        fetch_tests_from_squad_mock.return_value = None
        self.assertFalse(check(settings))
